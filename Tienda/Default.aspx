﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body bgcolor="#c4f4ff">
    <form id="form1" runat="server">
    <h1>Oscar es un Espinacas now()</h1><br />
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:catalogoConnectionString %>" 
        SelectCommand="SELECT [cod_categoria], [descripcion] FROM [categoria]">
    </asp:SqlDataSource>
    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" 
        DataSourceID="SqlDataSource1" DataTextField="descripcion" 
        DataValueField="cod_categoria">
    </asp:DropDownList>
    <br />
    <asp:Label ID="etiqueta" runat="server"></asp:Label>
    <br />
    <br />
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:catalogoConnectionString %>" 
        
        SelectCommand="SELECT caracteristica.caracteristica FROM aparato INNER JOIN cumple ON aparato.cod_aparato = cumple.cod_aparato INNER JOIN caracteristica ON cumple.cod_caracteristica = caracteristica.cod_caracteristica WHERE (aparato.cod_categoria = @Param1) GROUP BY caracteristica.caracteristica">
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownList1" Name="Param1" 
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" 
        DataSourceID="SqlDataSource2" DataTextField="caracteristica" 
        DataValueField="caracteristica" RepeatDirection="Horizontal">
    </asp:RadioButtonList>
    <br />
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
        ConnectionString="<%$ ConnectionStrings:catalogoConnectionString %>" 
        SelectCommand="SELECT aparato.cod_aparato, aparato.marca, aparato.modelo, aparato.cod_categoria, aparato.precio, caracteristica.caracteristica FROM aparato INNER JOIN cumple ON aparato.cod_aparato = cumple.cod_aparato INNER JOIN caracteristica ON cumple.cod_caracteristica = caracteristica.cod_caracteristica WHERE (aparato.cod_categoria = @Param1) AND (caracteristica.caracteristica = @Param2)">
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownList1" Name="Param1" 
                PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="RadioButtonList1" Name="Param2" 
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="GridView1" runat="server" AllowSorting="True" 
        AutoGenerateColumns="False" CellPadding="4" DataKeyNames="cod_aparato" 
        DataSourceID="SqlDataSource3" ForeColor="#333333" GridLines="None">
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:BoundField DataField="cod_aparato" HeaderText="cod_aparato" 
                ReadOnly="True" SortExpression="cod_aparato" />
            <asp:BoundField DataField="marca" HeaderText="marca" SortExpression="marca" />
            <asp:BoundField DataField="modelo" HeaderText="modelo" 
                SortExpression="modelo" />
            <asp:BoundField DataField="cod_categoria" HeaderText="cod_categoria" 
                SortExpression="cod_categoria" />
            <asp:BoundField DataField="precio" HeaderText="precio" 
                SortExpression="precio" />
            <asp:BoundField DataField="caracteristica" HeaderText="caracteristica" 
                SortExpression="caracteristica" />
            <asp:ImageField DataImageUrlField="cod_aparato" 
                DataImageUrlFormatString="img/{0}.jpg" HeaderText="Foto">
            </asp:ImageField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>
    <br />
    Email
    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
        Text="Quiero mas informacion" />
    <br />
    </form>
</body>
</html>
