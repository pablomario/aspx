﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Wasap</title>
</head>
<body>
    <form id="form1" runat="server">
    <h1>Wasap<asp:SqlDataSource ID="SqlDataSource1" runat="server" 
            ConnectionString="<%$ ConnectionStrings:wasapConnectionString %>" 
            SelectCommand="SELECT [num], [nombre] FROM [usuarios]"></asp:SqlDataSource>
    </h1>
    <p>
        <asp:Label ID="LabelUsuario" runat="server" Text="Usuario: "></asp:Label>
        <asp:DropDownList ID="DropUsuario" runat="server" AutoPostBack="True" 
            DataSourceID="SqlDataSource1" DataTextField="nombre" DataValueField="num">
        </asp:DropDownList>
    </p>
    <p>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:wasapConnectionString %>" 
            SelectCommand="SELECT nombre_grupo.nombre, nombre_grupo.n_grupo, grupos.num_usuario FROM grupos INNER JOIN nombre_grupo ON grupos.n_grupo = nombre_grupo.n_grupo WHERE (grupos.num_usuario = @Param1)">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropUsuario" Name="Param1" 
                    PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        <asp:Label ID="LabelGrupo" runat="server" Text="Grupo:"></asp:Label>
&nbsp;<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" 
            DataSourceID="SqlDataSource2" DataTextField="nombre" DataValueField="n_grupo">
        </asp:DropDownList>
    </p>
    <p>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
            ConnectionString="<%$ ConnectionStrings:wasapConnectionString %>" 
            
            SelectCommand="SELECT mensajes.texto, mensajes.cuando, usuarios.nombre, grupos.num_usuario FROM grupos INNER JOIN mensajes ON grupos.n_grupo = mensajes.grupo AND grupos.num_usuario = mensajes.quien INNER JOIN usuarios ON grupos.num_usuario = usuarios.num WHERE (grupos.n_grupo = @Param1)">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="Param1" 
                    PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="SqlDataSource3" AllowPaging="True" CellPadding="4" 
            ForeColor="#333333" GridLines="None" Height="100px" Width="100px">
            <RowStyle BackColor="#EFF3FB" />
            <Columns>
                <asp:BoundField DataField="texto" HeaderText="texto" SortExpression="texto" />
                <asp:BoundField DataField="cuando" HeaderText="cuando" 
                    SortExpression="cuando" />
                <asp:BoundField DataField="nombre" HeaderText="nombre" 
                    SortExpression="nombre" />
                <asp:ImageField DataImageUrlField="num_usuario" 
                    DataImageUrlFormatString="./img/{0}.jpg" >
                    <ControlStyle Height="150px" Width="150px" />
                    <ItemStyle Height="28px" Width="28px" />
                </asp:ImageField>
            </Columns>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </p>
    <p>
        <asp:TextBox ID="TextBox1" runat="server" Height="40px" Width="222px"></asp:TextBox>
&nbsp;<asp:Button ID="enviar" runat="server" Text="Enviar" onclick="enviar_Click" />
    </p>
    <p>&nbsp;</p>
    
</form>
    
</body>
</html>
